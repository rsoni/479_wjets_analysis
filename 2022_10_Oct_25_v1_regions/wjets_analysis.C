#define wjets_analysis_cxx
#include "wjets_analysis.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <math.h>
using namespace std;

void wjets_analysis::Loop()
{
//   In a ROOT session, you can do:
//      root> .L wjets_analysis.C
//      root> wjets_analysis t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();
   
   TH1F *h_met_signal = new TH1F("met_signal", "MET Signal Region;Missing Transverse Energy [GeV]; Number of events / GeV", 100, 0, 250);
   TH1F *h_met_control = new TH1F("met_control", "MET Control Region;Missing Transverse Energy [GeV]; Number of events / GeV", 100, 0, 250);
   TH1F *h_met_validation = new TH1F("met_validation", "MET Validation Region;Missing Transverse Energy [GeV]; Number of events / GeV", 100, 0, 250);
   TH1F *h_metmt_signal = new TH1F("metmt_signal", "MET + MtW Signal Region;Missing Transverse Mass (W) + Energy [GeV]; Number of events / GeV", 100, 0, 250);
   TH1F *h_metmt_validation = new TH1F("metmt_validation", "MET + MtW Validation Region;Missing Transverse Mass (W) + Energy [GeV]; Number of events / GeV", 100, 0, 250);
   TH1F *h_metmt_control = new TH1F("metmt_control", "MET + MtW Control Region;Missing Transverse Mass (W) + Energy [GeV]; Number of events / GeV", 100, 0, 250);
   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      if (el_pt->size() > 0)
      {
         double el_pt_0 = el_pt->front();
	 double el_m_0 = el_m->front();
	 double metFinalTrk_0 = metFinalTrk;
	 double el_phi_0 = el_phi->front();
	 double el_eta_0 = el_eta->front();
	 double metFinalTrkPhi_0 = metFinalTrkPhi;
	 TLorentzVector p4_met = TLorentzVector();
	 TLorentzVector p4_el = TLorentzVector();
	 p4_met.SetPtEtaPhiE(metFinalTrk_0, 0, metFinalTrkPhi_0, 0);
	 p4_el.SetPtEtaPhiE(el_pt_0, el_eta_0, el_phi_0, el_m_0);
	 double deltaphi = p4_met.DeltaPhi(p4_el);
	 double mtW = sqrt(2*el_pt_0*metFinalTrk_0*(1-cos(deltaphi)));
	 if (mtW + metFinalTrk_0 < 60 && metFinalTrk_0 > 25) {
		h_metmt_validation->Fill(mtW+metFinalTrk_0);
		h_met_validation->Fill(metFinalTrk_0);	
	 } 
	 if (mtW + metFinalTrk_0 > 60 && metFinalTrk_0 > 25){
		h_metmt_signal->Fill(mtW+metFinalTrk_0);
		h_met_signal->Fill(metFinalTrk_0);
	 }
	 if (metFinalTrk_0 < 25){
		h_met_control->Fill(metFinalTrk_0);
	 } 
	 h_metmt_control->Fill(mtW + metFinalTrk_0);
      }
      // if (Cut(ientry) < 0) continue;
   }
   TFile outfile ("Wjets_plots.root", "RECREATE") ;  
   TCanvas*c1 = new TCanvas();
   // Draw and save the histograms to pdf files

   h_metmt_validation->Draw();
   c1->SaveAs("h_metmt_validation.pdf");
   h_metmt_validation->Write();
   
   h_metmt_signal->Draw();
   c1->SaveAs("h_metmt_signal.pdf");
   h_metmt_signal->Write();
   
   h_metmt_control->Draw();
   c1->SaveAs("h_metmt_control.pdf");
   h_metmt_control->Write();
   
   h_met_validation->Draw();
   c1->SaveAs("h_met_validation.pdf");
   h_met_validation->Write();
   
   h_met_signal->Draw();
   c1->SaveAs("h_met_signal.pdf");
   h_metmt_signal->Write();
   
   h_met_control->Draw();
   c1->SaveAs("h_met_control.pdf");
   h_met_control->Write();

   outfile.Close();
}
