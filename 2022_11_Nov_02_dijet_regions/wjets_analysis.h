//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Thu Oct  6 12:36:07 2022 by ROOT version 6.24/02
// from TTree nominal/nominal
// found on file: user.jbossios.30668409._000006.tree.root
//////////////////////////////////////////////////////////

#ifndef wjets_analysis_h
#define wjets_analysis_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
#include "vector"

class wjets_analysis {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           mcEventNumber;
   Int_t           mcChannelNumber;
   Float_t         mcEventWeight;
   vector<float>   *mcEventWeights;
   Int_t           NPV;
   Float_t         actualInteractionsPerCrossing;
   Float_t         averageInteractionsPerCrossing;
   Float_t         weight_pileup;
   Float_t         correctedAverageMu;
   Float_t         correctedAndScaledAverageMu;
   Float_t         correctedActualMu;
   Float_t         correctedAndScaledActualMu;
   Float_t         weight_pileup_up;
   Float_t         weight_pileup_down;
   Int_t           rand_run_nr;
   Int_t           rand_lumiblock_nr;
   Int_t           pdgId1;
   Int_t           pdgId2;
   Int_t           pdfId1;
   Int_t           pdfId2;
   Float_t         x1;
   Float_t         x2;
   Float_t         q;
   Float_t         xf1;
   Float_t         xf2;
   vector<int>     *DFCommonJets_eventClean_LooseBad;
   vector<string>  *passedTriggers;
   vector<string>  *disabledTriggers;
   vector<float>   *el_m;
   vector<float>   *el_pt;
   vector<float>   *el_phi;
   vector<float>   *el_eta;
   vector<float>   *el_caloCluster_eta;
   vector<float>   *el_charge;
   vector<int>     *el_isIsolated_FCTight;
   vector<int>     *el_isIsolated_FCLoose;
   vector<int>     *el_DFCommonElectronsLHTight;
   vector<int>     *el_DFCommonElectronsLHMedium;
   vector<int>     *el_DFCommonElectronsLHLoose;
   vector<vector<float> > *el_PIDEff_SF_Tight;
   vector<vector<float> > *el_IsoEff_SF_Tight_isolFCTight;
   vector<vector<float> > *el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCTight;
   vector<vector<float> > *el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCTight;
   vector<vector<float> > *el_IsoEff_SF_Tight_isolFCLoose;
   vector<vector<float> > *el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCLoose;
   vector<vector<float> > *el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCLoose;
   vector<vector<float> > *el_RecoEff_SF;
   vector<int>     *el_trknSiHits;
   vector<int>     *el_trknPixHits;
   vector<int>     *el_trknPixHoles;
   vector<int>     *el_trknSCTHits;
   vector<int>     *el_trknSCTHoles;
   vector<int>     *el_trknTRTHits;
   vector<int>     *el_trknTRTHoles;
   vector<int>     *el_trknBLayerHits;
   vector<int>     *el_trknInnermostPixLayHits;
   vector<float>   *el_trkPixdEdX;
   vector<char>    *el_passSel;
   vector<char>    *el_passOR;
   vector<float>   *el_trkd0sig;
   vector<float>   *el_trkz0sintheta;
   vector<float>   *el_trkd0;
   vector<float>   *el_trkz0;
   vector<vector<int> > *el_isTrigMatchedToChain;
   vector<vector<string> > *el_listTrigChains;
   vector<float>   *jet_E;
   vector<float>   *jet_pt;
   vector<float>   *jet_phi;
   vector<float>   *jet_eta;
   vector<int>     *jet_JvtPass_Tight;
   vector<vector<float> > *jet_JvtEff_SF_Tight;
   vector<int>     *jet_fJvtPass_Loose;
   vector<vector<float> > *jet_fJvtEff_SF_Loose;
   vector<int>     *jet_is_DL1r_FixedCutBEff_85;
   vector<vector<float> > *jet_SF_DL1r_FixedCutBEff_85;
   vector<char>    *jet_passOR;
   vector<float>   *truthJet_E;
   vector<float>   *truthJet_pt;
   vector<float>   *truthJet_phi;
   vector<float>   *truthJet_eta;
   Float_t         metFinalTrk;
   Float_t         metFinalTrkPhi;
   vector<int>     *truthElectron_pdgId;
   vector<float>   *truthElectron_pt_dressed;
   vector<float>   *truthElectron_eta_dressed;
   vector<float>   *truthElectron_phi_dressed;
   vector<float>   *truthElectron_e_dressed;
   vector<unsigned int> *truthElectron_origin;
   vector<float>   *truthNeutrino_E;
   vector<float>   *truthNeutrino_pt;
   vector<float>   *truthNeutrino_phi;
   vector<float>   *truthNeutrino_eta;
   vector<int>     *truthNeutrino_pdgId;

   // List of branches
   TBranch        *b_mcEventNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mcEventWeight;   //!
   TBranch        *b_mcEventWeights;   //!
   TBranch        *b_NPV;   //!
   TBranch        *b_actualInteractionsPerCrossing;   //!
   TBranch        *b_averageInteractionsPerCrossing;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_correctedAverageMu;   //!
   TBranch        *b_correctedAndScaledAverageMu;   //!
   TBranch        *b_correctedActualMu;   //!
   TBranch        *b_correctedAndScaledActualMu;   //!
   TBranch        *b_weight_pileup_up;   //!
   TBranch        *b_weight_pileup_down;   //!
   TBranch        *b_rand_run_nr;   //!
   TBranch        *b_rand_lumiblock_nr;   //!
   TBranch        *b_pdgId1;   //!
   TBranch        *b_pdgId2;   //!
   TBranch        *b_pdfId1;   //!
   TBranch        *b_pdfId2;   //!
   TBranch        *b_x1;   //!
   TBranch        *b_x2;   //!
   TBranch        *b_q;   //!
   TBranch        *b_xf1;   //!
   TBranch        *b_xf2;   //!
   TBranch        *b_DFCommonJets_eventClean_LooseBad;   //!
   TBranch        *b_passedTriggers;   //!
   TBranch        *b_disabledTriggers;   //!
   TBranch        *b_el_m;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_caloCluster_eta;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_isIsolated_FCTight;   //!
   TBranch        *b_el_isIsolated_FCLoose;   //!
   TBranch        *b_el_DFCommonElectronsLHTight;   //!
   TBranch        *b_el_DFCommonElectronsLHMedium;   //!
   TBranch        *b_el_DFCommonElectronsLHLoose;   //!
   TBranch        *b_el_PIDEff_SF_Tight;   //!
   TBranch        *b_el_IsoEff_SF_Tight_isolFCTight;   //!
   TBranch        *b_el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCTight;   //!
   TBranch        *b_el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCTight;   //!
   TBranch        *b_el_IsoEff_SF_Tight_isolFCLoose;   //!
   TBranch        *b_el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCLoose;   //!
   TBranch        *b_el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCLoose;   //!
   TBranch        *b_el_RecoEff_SF;   //!
   TBranch        *b_el_trknSiHits;   //!
   TBranch        *b_el_trknPixHits;   //!
   TBranch        *b_el_trknPixHoles;   //!
   TBranch        *b_el_trknSCTHits;   //!
   TBranch        *b_el_trknSCTHoles;   //!
   TBranch        *b_el_trknTRTHits;   //!
   TBranch        *b_el_trknTRTHoles;   //!
   TBranch        *b_el_trknBLayerHits;   //!
   TBranch        *b_el_trknInnermostPixLayHits;   //!
   TBranch        *b_el_trkPixdEdX;   //!
   TBranch        *b_el_passSel;   //!
   TBranch        *b_el_passOR;   //!
   TBranch        *b_el_trkd0sig;   //!
   TBranch        *b_el_trkz0sintheta;   //!
   TBranch        *b_el_trkd0;   //!
   TBranch        *b_el_trkz0;   //!
   TBranch        *b_el_isTrigMatchedToChain;   //!
   TBranch        *b_el_listTrigChains;   //!
   TBranch        *b_jet_E;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_JvtPass_Tight;   //!
   TBranch        *b_jet_JvtEff_SF_Tight;   //!
   TBranch        *b_jet_fJvtPass_Loose;   //!
   TBranch        *b_jet_fJvtEff_SF_Loose;   //!
   TBranch        *b_jet_is_DL1r_FixedCutBEff_85;   //!
   TBranch        *b_jet_SF_DL1r_FixedCutBEff_85;   //!
   TBranch        *b_jet_passOR;   //!
   TBranch        *b_truthJet_E;   //!
   TBranch        *b_truthJet_pt;   //!
   TBranch        *b_truthJet_phi;   //!
   TBranch        *b_truthJet_eta;   //!
   TBranch        *b_metFinalTrk;   //!
   TBranch        *b_metFinalTrkPhi;   //!
   TBranch        *b_truthElectron_pdgId;   //!
   TBranch        *b_truthElectron_pt_dressed;   //!
   TBranch        *b_truthElectron_eta_dressed;   //!
   TBranch        *b_truthElectron_phi_dressed;   //!
   TBranch        *b_truthElectron_e_dressed;   //!
   TBranch        *b_truthElectron_origin;   //!
   TBranch        *b_truthNeutrino_E;   //!
   TBranch        *b_truthNeutrino_pt;   //!
   TBranch        *b_truthNeutrino_phi;   //!
   TBranch        *b_truthNeutrino_eta;   //!
   TBranch        *b_truthNeutrino_pdgId;   //!

   wjets_analysis(TTree *tree=0);
   virtual ~wjets_analysis();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef wjets_analysis_cxx
wjets_analysis::wjets_analysis(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("mc_dijet_files.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("mc_dijet_files.root");
      }
      TDirectory * dir = (TDirectory*)f->Get("mc_dijet_files.root:/TreeAlgo");
      dir->GetObject("nominal",tree);

   }
   Init(tree);
}

wjets_analysis::~wjets_analysis()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t wjets_analysis::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t wjets_analysis::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void wjets_analysis::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mcEventWeights = 0;
   DFCommonJets_eventClean_LooseBad = 0;
   passedTriggers = 0;
   disabledTriggers = 0;
   el_m = 0;
   el_pt = 0;
   el_phi = 0;
   el_eta = 0;
   el_caloCluster_eta = 0;
   el_charge = 0;
   el_isIsolated_FCTight = 0;
   el_isIsolated_FCLoose = 0;
   el_DFCommonElectronsLHTight = 0;
   el_DFCommonElectronsLHMedium = 0;
   el_DFCommonElectronsLHLoose = 0;
   el_PIDEff_SF_Tight = 0;
   el_IsoEff_SF_Tight_isolFCTight = 0;
   el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCTight = 0;
   el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCTight = 0;
   el_IsoEff_SF_Tight_isolFCLoose = 0;
   el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCLoose = 0;
   el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCLoose = 0;
   el_RecoEff_SF = 0;
   el_trknSiHits = 0;
   el_trknPixHits = 0;
   el_trknPixHoles = 0;
   el_trknSCTHits = 0;
   el_trknSCTHoles = 0;
   el_trknTRTHits = 0;
   el_trknTRTHoles = 0;
   el_trknBLayerHits = 0;
   el_trknInnermostPixLayHits = 0;
   el_trkPixdEdX = 0;
   el_passSel = 0;
   el_passOR = 0;
   el_trkd0sig = 0;
   el_trkz0sintheta = 0;
   el_trkd0 = 0;
   el_trkz0 = 0;
   el_isTrigMatchedToChain = 0;
   el_listTrigChains = 0;
   jet_E = 0;
   jet_pt = 0;
   jet_phi = 0;
   jet_eta = 0;
   jet_JvtPass_Tight = 0;
   jet_JvtEff_SF_Tight = 0;
   jet_fJvtPass_Loose = 0;
   jet_fJvtEff_SF_Loose = 0;
   jet_is_DL1r_FixedCutBEff_85 = 0;
   jet_SF_DL1r_FixedCutBEff_85 = 0;
   jet_passOR = 0;
   truthJet_E = 0;
   truthJet_pt = 0;
   truthJet_phi = 0;
   truthJet_eta = 0;
   truthElectron_pdgId = 0;
   truthElectron_pt_dressed = 0;
   truthElectron_eta_dressed = 0;
   truthElectron_phi_dressed = 0;
   truthElectron_e_dressed = 0;
   truthElectron_origin = 0;
   truthNeutrino_E = 0;
   truthNeutrino_pt = 0;
   truthNeutrino_phi = 0;
   truthNeutrino_eta = 0;
   truthNeutrino_pdgId = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mcEventNumber", &mcEventNumber, &b_mcEventNumber);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mcEventWeight", &mcEventWeight, &b_mcEventWeight);
   fChain->SetBranchAddress("mcEventWeights", &mcEventWeights, &b_mcEventWeights);
   fChain->SetBranchAddress("NPV", &NPV, &b_NPV);
   fChain->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing, &b_actualInteractionsPerCrossing);
   fChain->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing, &b_averageInteractionsPerCrossing);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("correctedAverageMu", &correctedAverageMu, &b_correctedAverageMu);
   fChain->SetBranchAddress("correctedAndScaledAverageMu", &correctedAndScaledAverageMu, &b_correctedAndScaledAverageMu);
   fChain->SetBranchAddress("correctedActualMu", &correctedActualMu, &b_correctedActualMu);
   fChain->SetBranchAddress("correctedAndScaledActualMu", &correctedAndScaledActualMu, &b_correctedAndScaledActualMu);
   fChain->SetBranchAddress("weight_pileup_up", &weight_pileup_up, &b_weight_pileup_up);
   fChain->SetBranchAddress("weight_pileup_down", &weight_pileup_down, &b_weight_pileup_down);
   fChain->SetBranchAddress("rand_run_nr", &rand_run_nr, &b_rand_run_nr);
   fChain->SetBranchAddress("rand_lumiblock_nr", &rand_lumiblock_nr, &b_rand_lumiblock_nr);
   fChain->SetBranchAddress("pdgId1", &pdgId1, &b_pdgId1);
   fChain->SetBranchAddress("pdgId2", &pdgId2, &b_pdgId2);
   fChain->SetBranchAddress("pdfId1", &pdfId1, &b_pdfId1);
   fChain->SetBranchAddress("pdfId2", &pdfId2, &b_pdfId2);
   fChain->SetBranchAddress("x1", &x1, &b_x1);
   fChain->SetBranchAddress("x2", &x2, &b_x2);
   fChain->SetBranchAddress("q", &q, &b_q);
   fChain->SetBranchAddress("xf1", &xf1, &b_xf1);
   fChain->SetBranchAddress("xf2", &xf2, &b_xf2);
   fChain->SetBranchAddress("DFCommonJets_eventClean_LooseBad", &DFCommonJets_eventClean_LooseBad, &b_DFCommonJets_eventClean_LooseBad);
   fChain->SetBranchAddress("passedTriggers", &passedTriggers, &b_passedTriggers);
   fChain->SetBranchAddress("disabledTriggers", &disabledTriggers, &b_disabledTriggers);
   fChain->SetBranchAddress("el_m", &el_m, &b_el_m);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_caloCluster_eta", &el_caloCluster_eta, &b_el_caloCluster_eta);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_isIsolated_FCTight", &el_isIsolated_FCTight, &b_el_isIsolated_FCTight);
   fChain->SetBranchAddress("el_isIsolated_FCLoose", &el_isIsolated_FCLoose, &b_el_isIsolated_FCLoose);
   fChain->SetBranchAddress("el_DFCommonElectronsLHTight", &el_DFCommonElectronsLHTight, &b_el_DFCommonElectronsLHTight);
   fChain->SetBranchAddress("el_DFCommonElectronsLHMedium", &el_DFCommonElectronsLHMedium, &b_el_DFCommonElectronsLHMedium);
   fChain->SetBranchAddress("el_DFCommonElectronsLHLoose", &el_DFCommonElectronsLHLoose, &b_el_DFCommonElectronsLHLoose);
   fChain->SetBranchAddress("el_PIDEff_SF_Tight", &el_PIDEff_SF_Tight, &b_el_PIDEff_SF_Tight);
   fChain->SetBranchAddress("el_IsoEff_SF_Tight_isolFCTight", &el_IsoEff_SF_Tight_isolFCTight, &b_el_IsoEff_SF_Tight_isolFCTight);
   fChain->SetBranchAddress("el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCTight", &el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCTight, &b_el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCTight);
   fChain->SetBranchAddress("el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCTight", &el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCTight, &b_el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCTight);
   fChain->SetBranchAddress("el_IsoEff_SF_Tight_isolFCLoose", &el_IsoEff_SF_Tight_isolFCLoose, &b_el_IsoEff_SF_Tight_isolFCLoose);
   fChain->SetBranchAddress("el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCLoose", &el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCLoose, &b_el_TrigEff_SF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCLoose);
   fChain->SetBranchAddress("el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCLoose", &el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCLoose, &b_el_TrigMCEff_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_Tight_isolFCLoose);
   fChain->SetBranchAddress("el_RecoEff_SF", &el_RecoEff_SF, &b_el_RecoEff_SF);
   fChain->SetBranchAddress("el_trknSiHits", &el_trknSiHits, &b_el_trknSiHits);
   fChain->SetBranchAddress("el_trknPixHits", &el_trknPixHits, &b_el_trknPixHits);
   fChain->SetBranchAddress("el_trknPixHoles", &el_trknPixHoles, &b_el_trknPixHoles);
   fChain->SetBranchAddress("el_trknSCTHits", &el_trknSCTHits, &b_el_trknSCTHits);
   fChain->SetBranchAddress("el_trknSCTHoles", &el_trknSCTHoles, &b_el_trknSCTHoles);
   fChain->SetBranchAddress("el_trknTRTHits", &el_trknTRTHits, &b_el_trknTRTHits);
   fChain->SetBranchAddress("el_trknTRTHoles", &el_trknTRTHoles, &b_el_trknTRTHoles);
   fChain->SetBranchAddress("el_trknBLayerHits", &el_trknBLayerHits, &b_el_trknBLayerHits);
   fChain->SetBranchAddress("el_trknInnermostPixLayHits", &el_trknInnermostPixLayHits, &b_el_trknInnermostPixLayHits);
   fChain->SetBranchAddress("el_trkPixdEdX", &el_trkPixdEdX, &b_el_trkPixdEdX);
   fChain->SetBranchAddress("el_passSel", &el_passSel, &b_el_passSel);
   fChain->SetBranchAddress("el_passOR", &el_passOR, &b_el_passOR);
   fChain->SetBranchAddress("el_trkd0sig", &el_trkd0sig, &b_el_trkd0sig);
   fChain->SetBranchAddress("el_trkz0sintheta", &el_trkz0sintheta, &b_el_trkz0sintheta);
   fChain->SetBranchAddress("el_trkd0", &el_trkd0, &b_el_trkd0);
   fChain->SetBranchAddress("el_trkz0", &el_trkz0, &b_el_trkz0);
   fChain->SetBranchAddress("el_isTrigMatchedToChain", &el_isTrigMatchedToChain, &b_el_isTrigMatchedToChain);
   fChain->SetBranchAddress("el_listTrigChains", &el_listTrigChains, &b_el_listTrigChains);
   fChain->SetBranchAddress("jet_E", &jet_E, &b_jet_E);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_JvtPass_Tight", &jet_JvtPass_Tight, &b_jet_JvtPass_Tight);
   fChain->SetBranchAddress("jet_JvtEff_SF_Tight", &jet_JvtEff_SF_Tight, &b_jet_JvtEff_SF_Tight);
   fChain->SetBranchAddress("jet_fJvtPass_Loose", &jet_fJvtPass_Loose, &b_jet_fJvtPass_Loose);
   fChain->SetBranchAddress("jet_fJvtEff_SF_Loose", &jet_fJvtEff_SF_Loose, &b_jet_fJvtEff_SF_Loose);
   fChain->SetBranchAddress("jet_is_DL1r_FixedCutBEff_85", &jet_is_DL1r_FixedCutBEff_85, &b_jet_is_DL1r_FixedCutBEff_85);
   fChain->SetBranchAddress("jet_SF_DL1r_FixedCutBEff_85", &jet_SF_DL1r_FixedCutBEff_85, &b_jet_SF_DL1r_FixedCutBEff_85);
   fChain->SetBranchAddress("jet_passOR", &jet_passOR, &b_jet_passOR);
   fChain->SetBranchAddress("truthJet_E", &truthJet_E, &b_truthJet_E);
   fChain->SetBranchAddress("truthJet_pt", &truthJet_pt, &b_truthJet_pt);
   fChain->SetBranchAddress("truthJet_phi", &truthJet_phi, &b_truthJet_phi);
   fChain->SetBranchAddress("truthJet_eta", &truthJet_eta, &b_truthJet_eta);
   fChain->SetBranchAddress("metFinalTrk", &metFinalTrk, &b_metFinalTrk);
   fChain->SetBranchAddress("metFinalTrkPhi", &metFinalTrkPhi, &b_metFinalTrkPhi);
   fChain->SetBranchAddress("truthElectron_pdgId", &truthElectron_pdgId, &b_truthElectron_pdgId);
   fChain->SetBranchAddress("truthElectron_pt_dressed", &truthElectron_pt_dressed, &b_truthElectron_pt_dressed);
   fChain->SetBranchAddress("truthElectron_eta_dressed", &truthElectron_eta_dressed, &b_truthElectron_eta_dressed);
   fChain->SetBranchAddress("truthElectron_phi_dressed", &truthElectron_phi_dressed, &b_truthElectron_phi_dressed);
   fChain->SetBranchAddress("truthElectron_e_dressed", &truthElectron_e_dressed, &b_truthElectron_e_dressed);
   fChain->SetBranchAddress("truthElectron_origin", &truthElectron_origin, &b_truthElectron_origin);
   fChain->SetBranchAddress("truthNeutrino_E", &truthNeutrino_E, &b_truthNeutrino_E);
   fChain->SetBranchAddress("truthNeutrino_pt", &truthNeutrino_pt, &b_truthNeutrino_pt);
   fChain->SetBranchAddress("truthNeutrino_phi", &truthNeutrino_phi, &b_truthNeutrino_phi);
   fChain->SetBranchAddress("truthNeutrino_eta", &truthNeutrino_eta, &b_truthNeutrino_eta);
   fChain->SetBranchAddress("truthNeutrino_pdgId", &truthNeutrino_pdgId, &b_truthNeutrino_pdgId);
   Notify();
}

Bool_t wjets_analysis::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void wjets_analysis::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t wjets_analysis::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef wjets_analysis_cxx
