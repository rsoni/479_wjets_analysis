#define wjets_analysis_cxx
#include "wjets_analysis.h"
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <iostream>
#include <math.h>
using namespace std;

void wjets_analysis::Loop()
{
//   In a ROOT session, you can do:
//      root> .L wjets_analysis.C
//      root> wjets_analysis t
//      root> t.GetEntry(12); // Fill t data members with entry number 12
//      root> t.Show();       // Show values of entry 12
//      root> t.Show(16);     // Read and show values of entry 16
//      root> t.Loop();       // Loop on all entries
//

//     This is the loop skeleton where:
//    jentry is the global entry number in the chain
//    ientry is the entry number in the current Tree
//  Note that the argument to GetEntry must be:
//    jentry for TChain::GetEntry
//    ientry for TTree::GetEntry and TBranch::GetEntry
//
//       To read only selected branches, Insert statements like:
// METHOD1:
//    fChain->SetBranchStatus("*",0);  // disable all branches
//    fChain->SetBranchStatus("branchname",1);  // activate branchname
// METHOD2: replace line
//    fChain->GetEntry(jentry);       //read all branches
//by  b_branchname->GetEntry(ientry); //read only this branch
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();
   TH1F *h_dphi_metel_signal_sum = new TH1F("dphi_metel_signal_sum", "Delta phi el-MET Signal (SUM) Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_metjet_signal_sum = new TH1F("dphi_metjet_signal_sum", "Delta phi jet-MET Signal (SUM) Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_eljet_signal_sum = new TH1F("dphi_eljet_signal_sum", "Delta phi el-jet Signal (SUM) Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_metel_signal_mt = new TH1F("dphi_metel_signal_mt", "Delta phi el-MET Signal (Mt) Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_eljet_signal_mt = new TH1F("dphi_eljet_signal_mt", "Delta phi el-jet Signal (Mt) Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_metjet_signal_mt = new TH1F("dphi_metjet_signal_mt", "Delta phi jet-MET Signal (Mt) Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_metel_validation_sum = new TH1F("dphi_metel_validation_sum", "Delta phi el-MET Validation (SUM) Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_metjet_validation_sum = new TH1F("dphi_metjet_validation_sum", "Delta phi jet-MET Validation (SUM) Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_eljet_validation_sum = new TH1F("dphi_eljet_validation_sum", "Delta phi el-jet Validation (SUM) Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_metel_validation_mt = new TH1F("dphi_metel_validation_mt", "Delta phi el-MET Validation (Mt) Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_metjet_validation_mt = new TH1F("dphi_metjet_validation_mt", "Delta phi jet-MET Validation (Mt) Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_eljet_validation_mt = new TH1F("dphi_eljet_validation_mt", "Delta phi el-jet Validation (Mt) Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_metel_preselection = new TH1F("dphi_metel_preselection", "Delta phi el-MET Preselection;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_metjet_preselection = new TH1F("dphi_metjet_preselection", "Delta phi jet-MET Preselection;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_eljet_preselection = new TH1F("dphi_eljet_preselection", "Delta phi el-jet Preselection;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_metel_control = new TH1F("dphi_metel_control", "Delta phi el-MET Control Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_metjet_control = new TH1F("dphi_metjet_control", "Delta phi jet-MET Control Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   TH1F *h_dphi_eljet_control = new TH1F("dphi_eljet_control", "Delta phi el-jet Control Region;Delta Phi [radians]; Number of events/GeV", 100, -3.4, 3.4);
   Long64_t nbytes = 0, nb = 0;
   for (Long64_t jentry=0; jentry<nentries;jentry++) {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      if (el_pt->size() > 0)
      {
	 double jet_pt_0 = jet_pt->front();
	 double jet_eta_0 = jet_eta->front();
	 double jet_phi_0 = jet_phi->front();
	 double jet_E_0 = jet_E->front();
         double el_pt_0 = el_pt->front();
	 double el_m_0 = el_m->front();
	 double metFinalTrk_0 = metFinalTrk;
	 double el_phi_0 = el_phi->front();
	 double el_eta_0 = fabs(el_eta->front());
	 double metFinalTrkPhi_0 = metFinalTrkPhi;
	 TLorentzVector p4_met = TLorentzVector();
	 TLorentzVector p4_el = TLorentzVector();
	 TLorentzVector p4_jet = TLorentzVector();
	 p4_met.SetPtEtaPhiE(metFinalTrk_0, 0, metFinalTrkPhi_0, 0);
	 p4_el.SetPtEtaPhiE(el_pt_0, el_eta_0, el_phi_0, el_m_0);
	 p4_jet.SetPtEtaPhiE(jet_pt_0, jet_eta_0, jet_phi_0, jet_E_0);
	 double deltaphi_metel = p4_met.DeltaPhi(p4_el);
	 double deltaphi_metjet = p4_met.DeltaPhi(p4_jet);
	 double deltaphi_eljet = p4_el.DeltaPhi(p4_jet);
	 double mtW = sqrt(2*el_pt_0*metFinalTrk_0*(1-cos(deltaphi_metel)));
	 h_dphi_metel_preselection->Fill(deltaphi_metel);
	 h_dphi_metjet_preselection->Fill(deltaphi_metjet);
	 h_dphi_eljet_preselection->Fill(deltaphi_eljet);
	 if (mtW + metFinalTrk_0 < 60 && metFinalTrk_0 > 25) {
		h_dphi_metel_validation_sum->Fill(deltaphi_metel);
		h_dphi_metjet_validation_sum->Fill(deltaphi_metjet);
		h_dphi_eljet_validation_sum->Fill(deltaphi_eljet);
	 } 
	 if (mtW < 60 && metFinalTrk_0 > 25) {
		h_dphi_metel_validation_mt->Fill(deltaphi_metel);
		h_dphi_metjet_validation_mt->Fill(deltaphi_metjet);
		h_dphi_eljet_validation_mt->Fill(deltaphi_eljet);
	 } 
	 if (mtW + metFinalTrk_0 > 60 && metFinalTrk_0 > 25){
		h_dphi_metel_signal_sum->Fill(deltaphi_metel);
		h_dphi_metjet_signal_sum->Fill(deltaphi_metjet);
		h_dphi_eljet_signal_sum->Fill(deltaphi_eljet);
	 }
	 if (mtW > 60 && metFinalTrk_0 > 25){
		h_dphi_metel_signal_mt->Fill(deltaphi_metel);
		h_dphi_metjet_signal_mt->Fill(deltaphi_metjet);
		h_dphi_eljet_signal_mt->Fill(deltaphi_eljet);
	 }
	 if (metFinalTrk_0 < 25){
		h_dphi_metel_control->Fill(deltaphi_metel);
		h_dphi_metjet_control->Fill(deltaphi_metjet);
		h_dphi_eljet_control->Fill(deltaphi_eljet);
	 } 
      }
      // if (Cut(ientry) < 0) continue;
   }
   TFile outfile ("Wjets_plots_data.root", "RECREATE") ;  
   TCanvas*c1 = new TCanvas();
   // Draw and save the histograms to pdf files

   h_dphi_metel_validation_sum->Draw();
   c1->SaveAs("h_dphi_metel_validation_sum.pdf");
   h_dphi_metel_validation_sum->Write();
   h_dphi_metel_validation_mt->Draw();
   c1->SaveAs("h_dphi_metel_validation_mt.pdf");
   h_dphi_metel_validation_mt->Write();
   
   h_dphi_metel_signal_sum->Draw();
   c1->SaveAs("h_dphi_metel_signal_sum.pdf");
   h_dphi_metel_signal_sum->Write();
   h_dphi_metel_signal_mt->Draw();
   c1->SaveAs("h_dphi_metel_signal_mt.pdf");
   h_dphi_metel_signal_mt->Write();
   
   h_dphi_metel_control->Draw();
   c1->SaveAs("h_dphi_metel_control.pdf");
   h_dphi_metel_control->Write();
   
   h_dphi_metjet_validation_sum->Draw();
   c1->SaveAs("h_dphi_metjet_validation_sum.pdf");
   h_dphi_metjet_validation_sum->Write();
   h_dphi_metjet_validation_mt->Draw();
   c1->SaveAs("h_dphi_metjet_validation_mt.pdf");
   h_dphi_metjet_validation_mt->Write();
   
   h_dphi_metjet_signal_sum->Draw();
   c1->SaveAs("h_dphi_metjet_signal_sum.pdf");
   h_dphi_metjet_signal_sum->Write();
   h_dphi_metjet_signal_mt->Draw();
   c1->SaveAs("h_dphi_metjet_signal_mt.pdf");
   h_dphi_metjet_signal_mt->Write();
   
   h_dphi_metjet_control->Draw();
   c1->SaveAs("h_dphi_metjet_control.pdf");
   h_dphi_metjet_control->Write();
   
   h_dphi_eljet_validation_sum->Draw();
   c1->SaveAs("h_dphi_eljet_validation_sum.pdf");
   h_dphi_eljet_validation_sum->Write();
   h_dphi_eljet_validation_mt->Draw();
   c1->SaveAs("h_dphi_eljet_validation_mt.pdf");
   h_dphi_eljet_validation_mt->Write();
   
   h_dphi_eljet_signal_sum->Draw();
   c1->SaveAs("h_dphi_eljet_signal_sum.pdf");
   h_dphi_eljet_signal_sum->Write();
   h_dphi_eljet_signal_mt->Draw();
   c1->SaveAs("h_dphi_eljet_signal_mt.pdf");
   h_dphi_eljet_signal_mt->Write();
   
   h_dphi_eljet_control->Draw();
   c1->SaveAs("h_dphi_eljet_control.pdf");
   h_dphi_eljet_control->Write();

   h_dphi_metel_preselection->Draw();
   c1->SaveAs("h_dphi_metel_preselection.pdf");
   h_dphi_metel_preselection->Write();

   h_dphi_metjet_preselection->Draw();
   c1->SaveAs("h_dphi_metjet_preselection.pdf");
   h_dphi_metjet_preselection->Write();
   
   h_dphi_eljet_preselection->Draw();
   c1->SaveAs("h_dphi_eljet_preselection.pdf");
   h_dphi_eljet_preselection->Write();


   outfile.Close();
}
